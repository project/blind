---SUMMARY---

This is a Drupal module that adds a Bcc address to all e-mails sent by the
website.


---INTRODUCTION---


Go to `/admin/config/system/site-information` and enter the e-mail address that 
should receive copies of all site e-mails.


---REQUIREMENTS---


None.


---INSTALLATION---


Download and enable as any other Drupal module. 


---CONFIGURATION---


Set up the address on admin/config/system/site-information.


---CONTACT---

Current Maintainers:
*Balogh Zoltán (zlyware) - https://www.drupal.org/u/u/zoltán-balogh
